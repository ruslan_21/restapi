<?php
/**
 * Created by PhpStorm.
 * User: ruslan
 * Date: 04.05.16
 * Time: 16:06
 */

namespace frontend\controllers;

use common\models\Post;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class PostController extends ActiveController
{
    public $modelClass = 'common\models\Post';

}
