<?php

namespace backend\controllers;

use common\models\Post;
use backend\models\PostApi;
use linslin\yii2\curl\Curl;
use Yii;
use yii\web\Controller;

class PostController extends Controller
{
    const CURL_LINK = 'http://rest.local/posts';
    const STATUS_ERROR = 422;
    const STATUS_POST_OK = 201;
    const STATUS_OK = 200;
    const STATUS_DELETE_OK = 204;
    const STATUS_NOT_FOUND = 404;

    public function actionIndex()
    {
        $curl = new Curl();

        $response = $curl->get(self::CURL_LINK);

        return $this->render('index',[
            'model' =>  json_decode($response)
        ]);
    }

    public function actionDelete($id)
    {
        $curl = new Curl();

        $curl->delete(self::CURL_LINK.'/'.$id);

        if($curl->responseCode == self::STATUS_DELETE_OK){
            return $this->redirect(['post/index']);
        }else{
            echo 'not found';
        }


    }

    public function actionView($id)
    {
        $curl = new Curl();

        $response = $curl->get(self::CURL_LINK.'/'.$id);

        if($curl->responseCode == self::STATUS_OK){
            return $this->render('view',[
                'model' =>  json_decode($response)
            ]);
        }else{
            echo 'not found';
        }

    }


    public function actionCreate()
    {
        $model = new PostApi();

        if ($model->load(Yii::$app->request->post())) {

            $curl = new Curl();
            $response = $curl->setOption(
                CURLOPT_POSTFIELDS,
                http_build_query(array(
                        'title' => $model->title,
                        'body' => $model->body,
                    )
                ))
                ->post(self::CURL_LINK);

            if($curl->responseCode == self::STATUS_ERROR){

                $errors = json_decode($response);

                foreach ($errors as $error) {
                    $model->addError($error->field, $error->message);
                }
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if($curl->responseCode == self::STATUS_POST_OK){
                return $this->redirect(['index']);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }



    public function actionUpdate($id)
    {
        $curl = new Curl();
        $response = json_decode($curl->get(self::CURL_LINK.'/'.$id));

        $model = new PostApi();
        $model->id = $response->id;
        $model->title = $response->title;
        $model->body = $response->body;

        if ($model->load(Yii::$app->request->post())) {

            $curl = new Curl();
            $response = $curl->setOption(
                CURLOPT_POSTFIELDS,
                http_build_query(array(
                        'title' => $model->title,
                        'body' => $model->body,
                    )
                ))
                ->put(self::CURL_LINK.'/'.$id);

            if($curl->responseCode == self::STATUS_ERROR){

                $errors = json_decode($response);

                foreach ($errors as $error) {
                    $model->addError($error->field, $error->message);
                }
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if($curl->responseCode == self::STATUS_OK){
                return $this->redirect(['index']);
            }


        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

}