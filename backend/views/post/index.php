<?php


/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Posts';
$this->params['breadcrumbs'][] = $this->title;

?>
<table class="table table-striped">
    <?php
    foreach($model as $value) {
        ?>
        <tr>
            <td>
                <?= $value->title;?>
            </td>
            <td>
                <a href=" <?= Url::to(['view', 'id' => $value->id])?>">Просмотр</a>
            </td>
            <td>
                <a href=" <?= Url::to(['delete', 'id' => $value->id])?>">Удалить</a>
            </td>
        </tr>
        <?php
    }
    ?>
</table>
<a href="<?= Url::to(['create'])?>" class="btn btn-primary">Создать</a>