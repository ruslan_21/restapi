<?php

/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Posts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<table class="table table-striped">

    <tr>
        <td>
           Название
        </td>
        <td>
            <?= $model->title;?>
        </td>
    </tr>
    <tr>
        <td>
            Контент
        </td>
        <td>
            <?= $model->body;?>
        </td>
    </tr>
</table>

<div class="row">
    <a href="<?= Url::to(['delete', 'id' => $model->id])?>" class="btn btn-warning">Удалить</a>
    <a href="<?= Url::to(['update', 'id' => $model->id])?>" class="btn btn-primary">Изменить</a>
</div>