<?php

use yii\db\Migration;

class m160504_125300_post extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('post', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'body' => $this->text(),
            'created_at' => $this->integer(11),

        ], $tableOptions);


        $post = [
            [
                'title' => 'запись 1',
                'body' => 'запись 1 текст ',
            ],
            [
                'title' => 'запись 2',
                'body' => 'запись 2 текст ',
            ],
            [
                'title' => 'запись 3',
                'body' => 'запись 3 текст ',
            ],
            [
                'title' => 'запись 4',
                'body' => 'запись 4 текст ',
            ],
            [
                'title' => 'запись 5',
                'body' => 'запись 5 текст ',
            ],
            [
                'title' => 'запись 6',
                'body' => 'запись 6 текст ',
            ],
            [
                'title' => 'запись 7',
                'body' => 'запись 7 текст ',
            ],
            [
                'title' => 'запись 8',
                'body' => 'запись 8 текст ',
            ],
            [
                'title' => 'запись 9',
                'body' => 'запись 9 текст ',
            ],
            [
                'title' => 'запись 10',
                'body' => 'запись 10 текст ',
            ],

        ];

        foreach ($post as $item) {
            $model = new \common\models\Post();
            $model->title = $item['title'];
            $model->body = $item['body'];
            $model->save();
        }
    }


    public function down()
    {
        $this->dropTable('post');
    }

}
