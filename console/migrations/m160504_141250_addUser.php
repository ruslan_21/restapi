<?php

use yii\db\Migration;

class m160504_141250_addUser extends Migration
{
    public function up()
    {
        $model = new \frontend\models\SignupForm();
        $model->username = 'admin';
        $model->password = 'admin123';
        $model->email = 'test@test.ru';
        $model->signup();
    }

    public function down()
    {
        $model = \common\models\User::find()->where(['username' => 'admin'])->one();
        $model->delete();
    }

}
